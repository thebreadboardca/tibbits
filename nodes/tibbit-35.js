module.exports = function (RED) {
    function Tibbit35Node(config) {
        RED.nodes.createNode(this, config);
        var node = this;
        var now = new Date();
	var nowMillis = Date.UTC(now.getUTCFullYear(),now.getUTCMonth(),now.getUTCDate(),now.getUTCHours(),now.getUTCMinutes(), 1);
    var lib = require("../../@tibbo-tps/tibbit-35/build/Release/tibbit35.node");


        this.on('input', function (msg) {
            	var newMsg = { topic:"", payload:""};
		var newMsg2 = { topic:"", payload:""};

		msg.topic = '{time:' + nowMillis/1000 + '; Slot:' + config.slot +'}';
		var data = new lib.PresData();
		var pres = new lib.Pressure();
		pres.getData(config.slot, data);
		

		newMsg.topic="pressure";
		newMsg2.topic="temperature";
		newMsg.payload = parseInt(data['pressure']);
		newMsg2.payload = parseInt(data['temperature']) ;
        	node.send([newMsg ,newMsg2]);
        });
    }
    RED.nodes.registerType("tibbit-35", Tibbit35Node);
}