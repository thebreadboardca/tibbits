module.exports = function (RED) {
    function Tibbit30Node(config) {
        RED.nodes.createNode(this, config);
        var node = this;
        var now = new Date();
	var nowMillis = Date.UTC(now.getUTCFullYear(),now.getUTCMonth(),now.getUTCDate(),now.getUTCHours(),now.getUTCMinutes(), 1);
    var lib = require("../../@tibbo-tps/tibbit-30/build/Release/tibbit30.node");


        this.on('input', function (msg) {
            	var newMsg = { topic:"", payload:""};
		var newMsg2 = { topic:"", payload:""};

		msg.topic = '{time:' + nowMillis/1000 + '; Slot:' + config.slot +'}';
            	var data = new lib.HumData();
            	var hum = new lib.Humidity();
            	hum.getData(config.slot, data);
		msg.payload = data ;
		
		newMsg.topic="humidity";
		newMsg2.topic="temperature";
		newMsg.payload = parseInt(data['humidity']);
		newMsg2.payload = parseInt(data['temperature']) ;
        	node.send([newMsg ,newMsg2]);
        });
    }
    RED.nodes.registerType("tibbit-30", Tibbit30Node);
}