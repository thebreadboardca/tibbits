module.exports = function (RED) {
    function Tibbit13Node(config) {
        RED.nodes.createNode(this, config);
        var node = this;

        var lib = require("../lib/ltps_adc.node");

        var adc = new lib.Adc();
        var data1 = new lib.AdcData();
        var data2 = new lib.AdcData();
        var data3 = new lib.AdcData();
        var data4 = new lib.AdcData();
        var failed = false;
        var failTXT = "";
        this.on('input', function (msg) {
            var newMsg1 = { topic: "1", payload: "" };
            var newMsg2 = { topic: "2", payload: "" };
            var newMsg3 = { topic: "3", payload: "" };
            var newMsg4 = { topic: "4", payload: "" };

            //adc.getVoltage(config.slot, parseInt(msg.topic), false, data1);
            adc.getVoltage(config.slot, 1, false, data1);
            if (data1.status == 1) { failed = true; failTXT += " " + data1.error;}
            adc.getVoltage(config.slot, 2, false, data2);
            if (data2.status == 1) { failed = true; failTXT += " " + data2.error; }
            adc.getVoltage(config.slot, 3, false, data3);
            if (data3.status == 1) { failed = true; failTXT += " " + data3.error; }
            adc.getVoltage(config.slot, 4, false, data4);
            if (data4.status == 1) { failed = true; failTXT += " " + data4.error; }
                          
            if (failed) 
            {
                this.status({ fill: "red", shape: "dot", text: "Failed" });
                this.Error(failTXT);
            }
            else {
                this.status({ fill: "blue", shape: "dot", text: "Sucess" }); 
                newMsg1.payload = data1.voltage;
                newMsg2.payload = data2.voltage;
                newMsg3.payload = data3.voltage;
                newMsg4.payload = data4.voltage;
            }
            node.send([newMsg1, newMsg2, newMsg3, newMsg4]);
        });
    }
    RED.nodes.registerType("tibbit-13", Tibbit13Node);
}