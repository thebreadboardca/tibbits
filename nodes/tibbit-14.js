module.exports = function (RED) {
    function Tibbit14Node(config) {
        RED.nodes.createNode(this, config);
        var node = this;

        var lib = require("../lib/ltps_dac.node");

        var dac = new lib.Dac();
        var data = new lib.DacData();

        this.on('input', function (msg) {

            dac.setVoltage(config.slot, parseInt(msg.topic), parseInt(msg.payload), data);
            if (data.status == 1)
                this.Error(data.error);
            else
                this.status("Voltage %d mV for channel %d applied successfully",  msg.payload,msg.topic);
            node.send(msg);
        });
    }
    RED.nodes.registerType("tibbit-14", Tibbit14Node);
}